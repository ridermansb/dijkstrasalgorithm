﻿using System;
using System.Linq;
using System.Linq.Expressions;
using DijkstraAlgorithm.Model;
using Raven.Client;

namespace DijkstraAlgorithm.Repository
{
	public class RavenDBRepository : IRepository
	{
		private readonly IDocumentSession _documentSession;
		public RavenDBRepository(IDocumentSession documentSession)
		{
			_documentSession = documentSession;
		}

		public void Store<T>(T obj)
			where T : IEntity
		{
			_documentSession.Store(obj);
		}

		public void SaveChanges()
		{
			_documentSession.SaveChanges();
		}


		public IQueryable<T> Query<T>(Expression<Func<T, bool>> filter = null)
			where T: IEntity
		{
			if (filter != null)
				return _documentSession.Query<T>().Where(filter);
			return _documentSession.Query<T>();
		}

		public T Get<T>(int id)
			where T: IEntity
		{
			return _documentSession.Load<T>(id);
		}
	}
}
