﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using DijkstraAlgorithm.Model;

namespace DijkstraAlgorithm.Repository
{
    public interface IRepository
    {
		void Store<T>(T obj) where T : IEntity;
		void SaveChanges();
		IQueryable<T> Query<T>(Expression<Func<T, bool>> filter = null) where T : IEntity;
		T Get<T>(int id) where T : IEntity;
    }
}
