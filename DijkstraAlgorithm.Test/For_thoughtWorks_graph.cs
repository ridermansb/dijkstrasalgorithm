﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace DijkstraAlgorithm.Test
{
	[TestClass]
	public class For_thoughtWorks_graph
	{
		private Graph graph;

		[TestInitialize]
		public void SetUp()
		{
			graph = new Graph("A", "B", "C", "D", "E");

			graph.AddEdge("A", "B", 5, false);
			graph.AddEdge("B", "C", 4, false);
			graph.AddEdge("C", "D", 8, true);
			graph.AddEdge("D", "E", 6, false);
			graph.AddEdge("A", "D", 5, false);
			graph.AddEdge("C", "E", 2, false);
			graph.AddEdge("E", "B", 3, false);
			graph.AddEdge("A", "E", 7, false);
		}

		[TestMethod]
		public void Distance_route_A_B_C_should_be_9()
		{
			var processResult = new MultiProcessGraph(graph, "A", "B", "C");

			Assert.AreEqual(processResult.Vertexes.Last().DistanceFromStart, 9);
		}

		[TestMethod]
		public void Distance_route_A_D_C_should_be_13()
		{
			var processStart = new MultiProcessGraph(graph, "A", "D", "C");
			Assert.AreEqual(processStart.Vertexes.Last().DistanceFromStart, 13);
		}

		[TestMethod]
		public void Distance_route_A_D_should_be_5()
		{
			var processStart = new MultiProcessGraph(graph, "A", "D");
			Assert.AreEqual(processStart.Vertexes.Last().DistanceFromStart, 5);
		}

		[TestMethod]
		public void Distance_route_A_E_B_C_D_should_be_22()
		{
			var processStart = new MultiProcessGraph(graph, "A", "E", "B", "C", "D");
			Assert.AreEqual(processStart.Vertexes.Last().DistanceFromStart, 22);
		}

		[TestMethod]
		public void Distance_route_A_E_D_should_be_22()
		{
			var processStart = new MultiProcessGraph(graph, "A", "E", "D");
			Assert.AreEqual(processStart.Vertexes.Last().DistanceFromStart, 22);
		}

		[TestMethod]
		public void Distance_route_A_C_should_be_9()
		{
			var processStart = new MultiProcessGraph(graph, "A", "C");
			Assert.AreEqual(processStart.Vertexes.Last().DistanceFromStart, 9);
		}

		[TestMethod]
		public void Path_route_A_E_D_should_be_A_E_B_C_D()
		{
			var processStart = new MultiProcessGraph(graph, "A", "E", "D");

			Assert.AreEqual("A", processStart.Vertexes.Single(p => p.Name == "E").FromVertex.Name);
			Assert.AreEqual("E", processStart.Vertexes.Single(p => p.Name == "B").FromVertex.Name);
			Assert.AreEqual("B", processStart.Vertexes.Single(p => p.Name == "C").FromVertex.Name);
			Assert.AreEqual("C", processStart.Vertexes.Single(p => p.Name == "D").FromVertex.Name);
		}

		[TestMethod]
		public void Path_route_C_D_A_should_be_invalid_route()
		{
			var processStart = new MultiProcessGraph(graph, "C", "D", "A");
			Assert.IsNull(processStart.Vertexes);
		}

		[TestMethod]
		public void Total_routes_C_C_should_be_2_distance_16_9() /* CEBCDC_CDCEBC_CDEBC_CEBCEBC_CEBCEBCEBC ?? */
		{
			var processStart = new AvailableRoutesProcessGraph(graph, "C");
			var avaliableRoutes = processStart.AvailableRoutesTo("C").ToList();

			Assert.AreEqual(2, avaliableRoutes.Count);

			Assert.AreEqual(16, avaliableRoutes[0].Last().DistanceFromStart);
			Assert.AreEqual(9, avaliableRoutes[1].Last().DistanceFromStart);
		}

		[TestMethod]
		public void Total_routes_B_B_should_be_1_distance_9()
		{
			var processStart = new AvailableRoutesProcessGraph(graph, "B");
			var avaliableRoutes = processStart.AvailableRoutesTo("B").ToList();

			Assert.AreEqual(1, avaliableRoutes.Count);
			Assert.AreEqual(9, avaliableRoutes[0].Last().DistanceFromStart);
		}

		[TestMethod]
		public void Total_routes_A_C_should_be_1_distance_9()
		{
			var processStart = new AvailableRoutesProcessGraph(graph, "A");
			var avaliableRoutes = processStart.AvailableRoutesTo("C").ToList();

			Assert.AreEqual(3, avaliableRoutes.Count);
			Assert.AreEqual(9, avaliableRoutes[0].Last().DistanceFromStart);
			Assert.AreEqual(13, avaliableRoutes[1].Last().DistanceFromStart);
			Assert.AreEqual(14, avaliableRoutes[2].Last().DistanceFromStart);
		}
	}
}
