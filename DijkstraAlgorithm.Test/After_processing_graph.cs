﻿using System.Collections.Generic;
using System.Linq;
using DijkstraAlgorithm;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DijkstraAlgorithm.Test
{
	[TestClass]
	public class After_processing_graph
	{
		private Graph graph;

		[TestInitialize]
		public void SetUp()
		{
			graph = new Graph("0", "1", "2", "3", "4", "5");

			graph.AddEdge("0", "1", 5, true);
			graph.AddEdge("0", "2", 10, true);

			graph.AddEdge("1", "2", 20, true);
			graph.AddEdge("1", "4", 30, true);

			graph.AddEdge("2", "4", 10, true);
			graph.AddEdge("2", "3", 15, true);

			graph.AddEdge("3", "4", 20, true);
			graph.AddEdge("3", "5", 40, true);

			graph.AddEdge("4", "5", 10, true);
		}

		[TestMethod]
		public void From_0_distance_to_5_should_be_30()
		{
			var processStart = new ProcessGraph(graph, "0");
			Assert.AreEqual(processStart.Vertexes.Last().DistanceFromStart, 30);
		}

		[TestMethod]
		public void From_0_to_5_path_should_be_0_2_4_5()
		{
			var processStart = new ProcessGraph(graph, "0");
			var list = processStart.Vertexes.Last().GetPath();

			Assert.ReferenceEquals(graph.Vertexes["0"], list[0]);
			Assert.ReferenceEquals(graph.Vertexes["2"], list[1]);
			Assert.ReferenceEquals(graph.Vertexes["4"], list[2]);
			Assert.ReferenceEquals(graph.Vertexes["5"], list[3]);
		}

		[TestMethod]
		public void From_0_to_5_should_be_0_2_4_5()
		{
			var processStart = new ProcessGraph(graph, "0");
			var listResult = processStart.PathTo("5");

			var listExpected = new List<VertexProcessed>() {
				processStart.Vertexes.Single(p => p.Name == "0")
				,processStart.Vertexes.Single(p => p.Name == "2")
				,processStart.Vertexes.Single(p => p.Name == "4")
				,processStart.Vertexes.Single(p => p.Name == "5")
			};

			CollectionAssert.AreEquivalent(listExpected, listResult.ToList());
		}

		[TestMethod]
		public void From_3_to_5_should_be_3_4_5()
		{
			var processStart = new ProcessGraph(graph, "3");
			var listResult = processStart.PathTo("5");

			var listExpected = new List<VertexProcessed>() {
				processStart.Vertexes.Single(p => p.Name == "3")
				,processStart.Vertexes.Single(p => p.Name == "4")
				,processStart.Vertexes.Single(p => p.Name == "5")
			};

			CollectionAssert.AreEquivalent(listExpected, listResult.ToList());
		}

		[TestMethod]
		public void From_1_to_4_should_be_1_2_4()
		{
			var processStart = new ProcessGraph(graph, "1");
			var listResult = processStart.PathTo("4");

			var listExpected = new List<VertexProcessed>() {
				processStart.Vertexes.Single(p => p.Name == "1")
				,processStart.Vertexes.Single(p => p.Name == "0")
				,processStart.Vertexes.Single(p => p.Name == "2")
				,processStart.Vertexes.Single(p => p.Name == "4")
			};

			CollectionAssert.AreEquivalent(listExpected, listResult.ToList());
		}
	}
}
