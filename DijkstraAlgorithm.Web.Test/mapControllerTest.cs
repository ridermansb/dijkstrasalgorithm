﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web.Mvc;
using DijkstraAlgorithm.Model;
using DijkstraAlgorithm.Repository;
using DijkstraAlgorithm.Web.Controllers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace DijkstraAlgorithm.Web.Test
{
	[TestClass]
	public class mapControllerTest
	{
		Mock<IRepository> mapRepository;
		mapController mapController;

		[TestInitialize]
		public void SetUp()
		{
			var mapList = new List<Map>{
					new Map() { Name = "ThoughtWorks Map" }
				 };

			mapRepository = new Mock<IRepository>();
			mapRepository.Setup(mapRep => mapRep.SaveChanges());
			mapRepository.Setup(mapRep => mapRep.Query<Map>(It.IsAny<Expression<Func<Map, Boolean>>>())).Returns(mapList.AsQueryable());


			mapController = new mapController(mapRepository.Object);
		}

		[TestMethod]
		public void Index_should_list_all_maps_by_default()
		{
			// Act
			var idxResult = (ViewResult)mapController.index(null);

			// Assert
			var list = idxResult.ViewData.Model as IList<Map>;
			Assert.IsNotNull(list, "Model should have been type of IList<Map>");
			Assert.AreEqual(1, list.Count);

		}

		[TestMethod]
		public void After_create_map_redirect_to_details()
		{
			//Arrange
			var map = new Map() { Name = "After_create_map_redirect_to_details" };

			// Act
			var viewResult = (RedirectToRouteResult)mapController.create(map);
			
			// Assert
			Assert.AreEqual("details", viewResult.RouteValues["action"]);

		}

		[TestMethod]
		public void After_create_map_must_be_in_the_repository()
		{
			//Arrange
			var map = new Map() { Name = "After_create_map_must_be_in_the_repository" };

			// Act
			var viewResult = (RedirectToRouteResult)mapController.create(map);
			
			// Assert
			mapRepository.Verify(rep => rep.SaveChanges(), Moq.Times.Once());
			mapRepository.Verify(rep => rep.Store(map), Moq.Times.Once());
		}
		
	}
}
	