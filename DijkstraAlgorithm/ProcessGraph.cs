﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace DijkstraAlgorithm
{
	public class ProcessGraph : IProcessGraph
	{
		public Graph Graph { get; private set; }
		public string StartVertex { get; private set; }
		public IEnumerable<VertexProcessed> Vertexes { get; private set; }

		public ProcessGraph(Graph graph, string startVertex)
		{
			Graph = graph;
			Vertexes = graph.Vertexes.Values.ToList().ConvertAll(v => new VertexProcessed(v.Name));
			foreach (var vertex in graph.Vertexes)
			{
				foreach (var edge in vertex.Value.Edges)
					Vertexes.Single(p => p.Name == vertex.Key).AddEdge(Vertexes.Single(p => p.Name == edge.Target.Name), edge.Distance, false);
			}
			StartVertex = startVertex;
			Process();
		}

		private void Process()
		{
			Vertexes.Single(p => p.Name == StartVertex).DistanceFromStart = 0;

			
			var queue = Vertexes.ToList();

			var processVertex = new Action<VertexProcessed>(vertex =>
																	{
																		var edges = vertex.Edges.Where(c => queue.Contains(c.Target));
																		foreach (var edge in edges)
																		{
																			double distance = vertex.DistanceFromStart + edge.Distance;
																			var vertexProc = (VertexProcessed)edge.Target;
																			if (distance < vertexProc.DistanceFromStart)
																			{
																				vertexProc.DistanceFromStart = distance;
																				vertexProc.FromVertex = vertex;
																			}
																		}
																	});

			while (true)
			{
				var nextVertex = queue.OrderBy(n => n.DistanceFromStart).FirstOrDefault(n => !double.IsPositiveInfinity(n.DistanceFromStart));
				if (nextVertex != null)
				{
					processVertex(nextVertex);
					queue.Remove(nextVertex);
					
				}
				else
					break;
			}
		}

		public IEnumerable<VertexProcessed> PathTo(string vertexTo)
		{
			if (!Vertexes.Any(v => v.Name == vertexTo.ToUpper())) throw new ApplicationException(string.Format("Vertex \"{0}\" does not exist in graph!", vertexTo));
			return Vertexes.Single(p => p.Name == vertexTo).GetPath().Reverse();
		}

		public double DistanceTo(string vertexTo)
		{
			if (!Vertexes.Any(v => v.Name == vertexTo.ToUpper())) throw new ApplicationException(string.Format("Vertex \"{0}\" does not exist in graph!", vertexTo));
			return Vertexes.Single(prop => prop.Name == vertexTo).DistanceFromStart;
		}
	}
}
