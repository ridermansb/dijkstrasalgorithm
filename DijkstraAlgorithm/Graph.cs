﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace DijkstraAlgorithm
{
	[DebuggerDisplay("Graph - ({Vertexes.Count} vertexes)")]
	public class Graph
	{
		public IDictionary<string, IVertex> Vertexes { get; private set; }

		public Graph(params string[] vertexes)
		{
			Vertexes = new Dictionary<string, IVertex>();
			foreach (var item in vertexes)
				AddVertex(item);
		}

		public void AddVertex(string name)
		{
			Vertexes.Add(name, new Vertex(name));
		}
		public void AddEdge(string fromVertex, string toVertex, int distance, bool twoWay)
		{
			Vertexes[fromVertex].AddEdge(Vertexes[toVertex], distance, twoWay);
		}
	}
}
