﻿using System.Collections.Generic;
using System.Linq;

namespace DijkstraAlgorithm
{
	public class MultiProcessGraph : IProcessGraph
	{
		public IEnumerable<VertexProcessed> Vertexes { get; private set; }
		public Graph Graph { get; private set; }

		public MultiProcessGraph(Graph graph, params string[] vertexes)
		{
			Graph = graph;
			var vertextProcessVertexes = new List<VertexProcessed>();
			for (int i = 0; i < vertexes.Length - 1; i++)
			{
				var vNext = vertexes.GetValue((i + 1) % vertexes.Length).ToString();

				var process = new ProcessGraph(graph, vertexes[i]);

				var path = process.PathTo(vNext).ToList();

				if (vertextProcessVertexes.Count() > 0 && vertextProcessVertexes.Last().Name == path.First().Name)
				{
					path.RemoveAt(0);
					path.ForEach(a => a.DistanceFromStart += vertextProcessVertexes.Last().DistanceFromStart);
				}

				foreach (var vPath in path)
				{
					if (double.IsPositiveInfinity(vPath.DistanceFromStart))
					{
						vertextProcessVertexes = null;
						break;
					}
					vertextProcessVertexes.Add(vPath);
				}
			}

			Vertexes = vertextProcessVertexes.AsEnumerable();
		}
	}
}
