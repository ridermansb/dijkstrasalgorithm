﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace DijkstraAlgorithm
{
	[DebuggerDisplay("Vertex {Name} - (Edges: {Edges.Count})")]
	public class Vertex : IVertex
	{
		public string Name { get; private set; }
		public IList<Edge> Edges { get; private set; }

		public Vertex(string name)
		{
			Name = name;
			Edges = new List<Edge>();
		}

		public void AddEdge(IVertex targetVertex, double distance, bool twoWay)
		{
			if (targetVertex == null) throw new ArgumentNullException("targetVertex");
			if (targetVertex == this) throw new ArgumentException("Vertex may not connect to itself.");
			if (distance <= 0) throw new ArgumentException("Distance must be positive.");

			Edges.Add(new Edge(targetVertex, distance));
			if (twoWay)
				targetVertex.AddEdge(this, distance, false);
		}
	}
}
