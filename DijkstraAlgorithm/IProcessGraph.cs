﻿using System.Collections.Generic;

namespace DijkstraAlgorithm
{
	public interface IProcessGraph
	{
		Graph Graph { get; }
		IEnumerable<VertexProcessed> Vertexes { get; }
	}
}
