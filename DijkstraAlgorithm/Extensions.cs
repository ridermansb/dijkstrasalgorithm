﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DijkstraAlgorithm
{
	public static class Extensions
	{
		public static IDictionary<string, T> Clone<T>(this IDictionary<string, IVertex> vertexes, Func<IVertex, T> vertexCreate)
			where T: IVertex
		{
			IDictionary<string, T> vertexesInitializaded = vertexes.ToDictionary(k => k.Key, v => vertexCreate(v.Value));

			// Copy Edges
			foreach (var vertex in vertexes)
			{
				foreach (var edge in vertex.Value.Edges)
					vertexesInitializaded[vertex.Key].AddEdge(vertexes[edge.Target.Name], edge.Distance, false);
			}

			return vertexesInitializaded;
		}

	}
}
