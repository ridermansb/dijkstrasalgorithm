﻿using System.Diagnostics;

namespace DijkstraAlgorithm
{
	[DebuggerDisplay("{Target.Name} - (Distance: {Distance})")]
	public class Edge
	{
		public IVertex Target { get; private set; }
		public double Distance { get; private set; }

		public Edge(IVertex target, double distance)
		{
			Target = target;
			Distance = distance;
		}
	}
}
