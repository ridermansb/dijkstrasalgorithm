﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace DijkstraAlgorithm
{
	public class AvailableRoutesProcessGraph : IProcessGraph
	{
		public Graph Graph { get; private set; }
		public IEnumerable<VertexProcessed> Vertexes 
		{ 
			get { throw new ApplicationException("Please, use AvailableRoutesProcessGraph method!"); } 
		}
		public string StartVertex { get; set; }

		public AvailableRoutesProcessGraph(Graph graph, string vertexFrom)
		{
			if (!graph.Vertexes.Any(v => v.Key == vertexFrom.ToUpper())) throw new ApplicationException(string.Format("Vertex \"{0}\" does not exist in graph!", vertexFrom));
			StartVertex = vertexFrom;

			Graph = graph;
		}

		public ICollection<IEnumerable<VertexProcessed>> AvailableRoutesTo(string vertexTo)
		{
			var routes = new Collection<IEnumerable<VertexProcessed>>();

			var vertex = Graph.Vertexes.Single(p => p.Key == StartVertex);
			foreach (var edge in vertex.Value.Edges)
			{
				var m = new MultiProcessGraph(Graph, StartVertex, edge.Target.Name, vertexTo);
				routes.Add(m.Vertexes);
			}

			return routes;
		}
	}
}
