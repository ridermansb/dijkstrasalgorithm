﻿using System.Collections.Generic;

namespace DijkstraAlgorithm
{
	public interface IVertex
	{
		string Name { get; }
		IList<Edge> Edges { get; }
		void AddEdge(IVertex targetVertex, double distance, bool twoWay);
	}
}
