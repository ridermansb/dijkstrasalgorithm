﻿using System.Collections.Generic;
using System.Diagnostics;

namespace DijkstraAlgorithm
{
	[DebuggerDisplay("Vertex {Name} - (Edges: {Edges.Count}, Distance: {DistanceFromStart}, From Vertex: {FromVertex.Name})")]
	public sealed class VertexProcessed : Vertex
	{
		public double DistanceFromStart { get; set; }
		public VertexProcessed FromVertex { get; set; }

		public VertexProcessed(string name)
			: base(name)
		{
			DistanceFromStart = double.PositiveInfinity;
		}

		public IList<VertexProcessed> GetPath()
		{
			var listPath = new List<VertexProcessed>();
			listPath.Add(this);
			if (FromVertex != null)
				listPath.AddRange(FromVertex.GetPath());
			return listPath;
		}
	}
}
