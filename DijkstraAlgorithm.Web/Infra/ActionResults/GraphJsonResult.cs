﻿using System;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using System.Linq;
using DijkstraAlgorithm.Web.Extension;
using AppfailReporting;

namespace DijkstraAlgorithm.Web.Infra.ActionResults
{
	public class GraphProcessedJsonResult : JsonResult
	{
		private IProcessGraph _graphProcessed;
		public GraphProcessedJsonResult(IProcessGraph graphProcessed)
		{
			_graphProcessed = graphProcessed;
		}

		public override void ExecuteResult(ControllerContext context)
		{
			if (context == null)
				throw new ArgumentNullException("context");

			var response = context.HttpContext.Response;

			response.ContentType = ContentType ?? "application/json";

			if (ContentEncoding != null)
				response.ContentEncoding = ContentEncoding;

			try
			{
				if (_graphProcessed.Vertexes == null)
				{
					response.StatusCode = 404;
					response.StatusDescription = "No such route";
				}
				else
				{
					Data = (from v in _graphProcessed.Vertexes
							select new
							{
								v.Name
								,
								FromVertex = (v.FromVertex != null ? v.FromVertex.Name : null)
								,
								DistanceFromStart = (double.IsPositiveInfinity(v.DistanceFromStart) ? null : new Nullable<double>(v.DistanceFromStart))
								,
								Edges = v.Edges.Select(e => new { TargeName = e.Target.Name, e.Distance }).ToList()
							}).ToList();
				}
			}
			catch (Exception ex)
			{
				response.StatusCode = 500;
				response.StatusDescription = ex.GetInnerException().Message;
				ex.SendToAppfail(context.HttpContext);
			}

			base.ExecuteResult(context);
			
		}
	}
}