﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using DijkstraAlgorithm.Model;
using DijkstraAlgorithm.Repository;
using MvcSiteMapProvider.Extensibility;

namespace DijkstraAlgorithm.Web.Infra.Sitemap
{
	public class MapDynamicNodeProvider : DynamicNodeProviderBase
	{
		#region Overrides of DynamicNodeProviderBase

		public override IEnumerable<DynamicNode> GetDynamicNodeCollection()
		{
			var session = DependencyResolver.Current.GetService<IRepository>();

			var listDB = (from p in session.Query<Map>()
						  orderby p.DateAdded descending
						  select new
						  {
							  p.Name
							  ,
							  p.Id
						  });

			foreach (var map in listDB.ToList())
			{
				var root = new DynamicNode()
				{
					Title = map.Name
					,Description="map"
				};
				root.RouteValues.Add("id", map.Id);
				yield return root;
			}
		}

		#endregion
	}
}