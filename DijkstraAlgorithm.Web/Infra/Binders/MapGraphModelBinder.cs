﻿using System.Linq;
using System.Web.Mvc;
using Newtonsoft.Json.Linq;

namespace DijkstraAlgorithm.Web.Infra.Binders
{
	public class GraphModelBinder : IModelBinder
	{
		public object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
		{
			var graph = new Graph();
			var json = bindingContext.ValueProvider.GetValue("graph").AttemptedValue.ToString();

			var jsonLinq = JObject.Parse(json);

			var vertexes = from jobj in jsonLinq.Descendants()
						   where jobj.Type == JTokenType.Property && (
							   ((JProperty)jobj).Name == "shapeType"
							   && ((JProperty)jobj).Value.Value<string>() == "Circle"
						   )
						   select jobj.Parent["attrs"];
			foreach (var vertex in vertexes)
				graph.AddVertex(vertex["id"].Value<string>());

			var edges = from jobj in jsonLinq.Descendants()
						where jobj.Type == JTokenType.Property && (
							((JProperty)jobj).Name == "shapeType"
							&& ((JProperty)jobj).Value.Value<string>() == "Line"
						)
						select jobj.Parent["attrs"];
			foreach (var edge in edges)
				graph.AddEdge(edge["fromVertex"].Value<string>(), edge["toVertex"].Value<string>(), edge["distance"].Value<int>(), false);

			return graph;
		}
	}
}