using System.IO;
using System.Text.RegularExpressions;
using Cassette.Configuration;
using Cassette.Scripts;
using Cassette.Stylesheets;

namespace DijkstraAlgorithm.Web
{
    /// <summary>
    /// Configures the Cassette asset modules for the web application.
    /// </summary>
    public class CassetteConfiguration : ICassetteConfiguration
    {
        public void Configure(BundleCollection bundles, CassetteSettings settings)
        {
			#region CDNs

			bundles.AddUrlWithLocalAssets(
				"http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js",
				new LocalAssetSettings
				{
					Path = "~/Scripts/jquery-1.7.2.min.js",
					FallbackCondition = "!window.jQuery"
				}
				);

			bundles.AddUrlWithLocalAssets(
				"http://ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.min.js",
				new LocalAssetSettings
				{
					Path = "~/Scripts/jquery.validate.min.js",
					FallbackCondition = "typeof jQuery.validator == 'undefined'",
				}
				);

			bundles.AddUrlWithAlias(
				"http://ajax.aspnetcdn.com/ajax/mvc/3.0/jquery.validate.unobtrusive.min.js",
				"jquery-validate-unobtrusive"
				);

			#endregion

			#region Global

			bundles.Add<StylesheetBundle>("Content", new FileSearch
			{
				SearchOption = SearchOption.TopDirectoryOnly,
				Pattern = "*.css",
				Exclude = new Regex(@"_base.*\.css$")
				,
			}, b => b.Processor = new StylesheetPipeline
			{
				CompileSass = false
			});

			bundles.Add<ScriptBundle>("Scripts", new FileSearch
			{
				SearchOption = SearchOption.TopDirectoryOnly,
				Pattern = "*.js",
				Exclude = new Regex("selectivizr-min.js$" +
									@"|jquery-1.7.2.*\.js$" +
									@"|jquery.validate.*\.js$" +
									@"|.*debug.js$" +
									@"|.*vsdoc.js$")
			});

			#endregion

			#region Per page

			bundles.AddPerIndividualFile<ScriptBundle>("Scripts/Controllers", null, bundle => bundle.PageLocation = "page");
			bundles.AddPerIndividualFile<StylesheetBundle>("Content/Controllers", null, bundle =>
			{
				bundle.PageLocation = "page";
				bundle.Processor = new StylesheetPipeline
				{
					CompileSass = false
				};
			});

			#endregion

			#region Vendors

			bundles.Add<ScriptBundle>("/Scripts/Vendors/kinetic-v4.4.3.min.js", b => b.PageLocation = "top");
			bundles.Add<ScriptBundle>("/Scripts/Vendors/jquery.buildr.min.js", b => b.PageLocation = "top");

			#endregion
        }
    }
}