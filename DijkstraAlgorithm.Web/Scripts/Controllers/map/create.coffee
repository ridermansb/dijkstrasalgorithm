nextChaIdent = 'A'
currentShapeOver = null
currentShapeSelected = null

setStyleOver = (groupShape) ->
	for shape in groupShape.children
		shape.setFill('#333')
	groupShape.transitionTo
		scale:
			x: 1.3
			y: 1.3
		duration: 1
		easing: 'elastic-ease-out'
	layer.draw()

setStyleOut = (groupShape) ->
	for shape in groupShape.children
		shape.setFill('#666')
	groupShape.transitionTo
		scale:
			x: 1
			y: 1
		duration: 1
		easing: 'elastic-ease-out'
	layer.draw()

getNextCha = ->
	nextChaIdent = String.fromCharCode(nextChaIdent.charCodeAt(0) + 1)
	vertex = layer.get("#group_#{nextChaIdent}")
	if vertex.length > 0
		nextChaIdent = getNextCha()
	nextChaIdent

process = (from, to, distance) ->
	$map = $("#map")

	x = Math.floor((Math.random()*$map.width()-15)+15);
	y = Math.floor((Math.random()*$map.height()-15)+15);
	fromVertex = layer.get("#group_#{from}")
	if fromVertex.length > 0
		fromVertex = fromVertex[0]
	else
		fromVertex = addVertex from,
			x: x
			y: y

	x = Math.floor((Math.random()*$map.width()-15)+15);
	y = Math.floor((Math.random()*$map.height()-15)+15);
	toVertex = layer.get("#group_#{to}")
	if toVertex.length > 0
		toVertex = toVertex[0]
	else
		toVertex = addVertex to,
			x: x
			y: y

	addEdge fromVertex, toVertex, distance

addEdge = (shapeFrom, shapeTo, distance) -> 
	posOrigem = shapeFrom.getPosition()
	posDest = shapeTo.getPosition()
	posCenter =
		x: (posDest.x + posOrigem.x) / 2
		y: (posDest.y + posOrigem.y) / 2

	nameFrom = shapeFrom.children[0].getId()
	nameTo = shapeTo.children[0].getId()

	
	#Two ways?
	lineTwoWay = stage.get("#edge_#{nameTo}#{nameFrom}")
	if lineTwoWay && lineTwoWay.length > 0
		lineTwoWay = lineTwoWay[0]
		distance = parseInt lineTwoWay.attrs.distance if !distance
		text = stage.get("#textDistance_#{nameTo}#{nameFrom}")
	else
		lineTwoWay = null

	if !lineTwoWay and !distance
		distance = Math.sqrt(Math.pow(posDest.x - posOrigem.x, 2) + Math.pow(posDest.y - posOrigem.y, 2))
		distance = Math.round(distance)
		distance = prompt("Distance between vertices: ", distance) if $("#prompt-distances > input").is(":checked")

	line = new Kinetic.Line
		stroke: "#08C"
		strokeWidth: 6
		points: [posOrigem.x, posOrigem.y, posDest.x, posDest.y]
		name: "edge"
		id: "edge_#{nameFrom}#{nameTo}"
	line.setAttrs
		distance: distance
		fromVertex: nameFrom
		toVertex: nameTo

	# Update text if two ways
	if lineTwoWay and text and text.length > 0
		text[0].setText("#{nameTo} <> #{nameFrom}: #{distance}")
	else
		text = new Kinetic.Text
			x: posCenter.x
			y: posCenter.y
			text: "#{nameFrom} > #{nameTo}: #{distance}"
			fontSize: 10
			fontFamily: "Calibri"
			stroke: "#333"
			cornerRadius: 5
			strokeWidth: 1
			textFill: "#fff"
			fill: "#08C"
			align: "center"
			padding: 2
			id: "textDistance_#{nameFrom}#{nameTo}"
		layer.add text

	layer.add line
	line.moveToBottom()
	layer.draw()

	# Clean
	setStyleOut(shapeFrom)
	setStyleOut(shapeTo)
	currentShapeSelected = null

addVertex = (letter = nextChaIdent, posMouse = stage.getMousePosition()) ->
	return if currentShapeOver
	
	posX = posMouse.x
	posY = posMouse.y

	group = new Kinetic.Group
		x: posX
		y: posY
		id: "group_#{letter}"
	circle = new Kinetic.Circle
		radius: 20
		fill: "#666"
		stroke: "black"
		strokeWidth: 1
		name: "vertex"
		id: letter
	text = new Kinetic.Text
		x: -5
		y: -5
		text: letter
		fontSize: 12
		fontFamily: "Calibri"
		fontStyle: "bold"
		textFill: "#fff"
		align: "center"
		id: "text_#{letter}"

	group.add circle
	group.add text
	
	group.on("mouseover touchstart", (evt) -> 
		setStyleOver(@)
		currentShapeOver = @
	)
	group.on("mouseout touchend", (evt) -> 
		currentShapeOver = null
		setStyleOut(@) if (currentShapeSelected and currentShapeSelected.getId() != @.getId()) or !currentShapeSelected
	)

	group.on("click tap", (evt) -> 
		if currentShapeSelected 
			if currentShapeSelected.getId() != @.getId()
				addEdge(currentShapeSelected, @)
			else
				setStyleOut(currentShapeSelected)
				currentShapeSelected = null
		else
			setStyleOver @
			currentShapeSelected = currentShapeOver
	)

	layer.add group
	stage.draw()
	nextChaIdent = getNextCha()

	group

$("#prompt-distances").tooltip
	title: "Check to enable prompt distance"

$("#form-map button").click (e) -> 
	e.preventDefault()
	$("#GraphJson").val stage.toJSON()
	$("#form-map").submit()

$("#reset-map").click (e) -> 
	e.preventDefault()
	layer.removeChildren()
	nextChaIdent = 'A'
	currentShapeOver = null
	currentShapeSelected = null
	layer.draw()

$("#text-graph")
	.bind("keyup change", (e) ->
		if (e.type == "keyup" and e.keyCode == 13) or e.type == "change"
			edges = e.target.value.split(/(?:,| )+/)
			for e in edges
				distance = e.replace(/[^\d.]/g, "")
				e = e.replace(/[\d.]/g, "")
				process e[0], e[1], distance

	)


stage = new Kinetic.Stage
	container: "map"
	width: $("#sitemap-path").outerWidth()
	height: 500
layer = new Kinetic.Layer()
stage.add(layer)
stage.getContainer().addEventListener("click", () -> addVertex())