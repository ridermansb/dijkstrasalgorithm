(function() {
  var calculeRoute, currentShapeOver, currentShapeSelected, item, processShape, setStyleOut, setStyleOver, stage, _i, _len, _ref;

  currentShapeOver = null;

  currentShapeSelected = null;

  setStyleOver = function(groupShape) {
    var shape, _i, _len, _ref;
    _ref = groupShape.children;
    for (_i = 0, _len = _ref.length; _i < _len; _i++) {
      shape = _ref[_i];
      shape.setFill('#333');
    }
    groupShape.transitionTo({
      scale: {
        x: 1.3,
        y: 1.3
      },
      duration: 1,
      easing: 'elastic-ease-out'
    });
    return stage.draw();
  };

  setStyleOut = function(groupShape) {
    var shape, _i, _len, _ref;
    _ref = groupShape.children;
    for (_i = 0, _len = _ref.length; _i < _len; _i++) {
      shape = _ref[_i];
      shape.setFill('#666');
    }
    groupShape.transitionTo({
      scale: {
        x: 1,
        y: 1
      },
      duration: 1,
      easing: 'elastic-ease-out'
    });
    return stage.draw();
  };

  calculeRoute = function(shapeTo) {
    console.log("Calculing route");
    $.get("/map/route", {
      id: $("#Id").val(),
      from: currentShapeSelected.getName(),
      to: shapeTo.getName()
    }, function(data) {
      return console.dir(data);
    });
    setStyleOut(currentShapeSelected);
    setStyleOut(shapeTo);
    return currentShapeSelected = null;
  };

  processShape = function(shape) {
    var item, _i, _len, _ref;
    if (shape.children) {
      _ref = shape.children;
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        item = _ref[_i];
        processShape(item);
      }
    }
    if (shape.nodeType === "Group") {
      shape.on("mouseover touchstart", function(evt) {
        setStyleOver(this);
        return currentShapeOver = this;
      });
      shape.on("mouseout touchend", function(evt) {
        currentShapeOver = null;
        if ((currentShapeSelected && currentShapeSelected.getId() !== this.getId()) || !currentShapeSelected) {
          return setStyleOut(this);
        }
      });
      return shape.on("click tap", function(evt) {
        if (currentShapeSelected) {
          if (currentShapeSelected.getId() !== this.getId()) {
            return calculeRoute(this);
          } else {
            setStyleOut(currentShapeSelected);
            return currentShapeSelected = null;
          }
        } else {
          setStyleOver(this);
          return currentShapeSelected = currentShapeOver;
        }
      });
    }
  };

  stage = new Kinetic.Stage({
    container: "map",
    width: $("#sitemap-path").outerWidth(),
    height: 500
  });

  stage.load($("#GraphJson").val());

  _ref = stage.children;
  for (_i = 0, _len = _ref.length; _i < _len; _i++) {
    item = _ref[_i];
    processShape(item);
  }

}).call(this);
