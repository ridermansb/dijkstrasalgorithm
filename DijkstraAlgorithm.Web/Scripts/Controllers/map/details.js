﻿(function() {
  var cleanupRoutes, displayMessage, drawRoute, item, processGraph, processShape, selectedVertexes, setStyleOut, setStyleOver, stage, textInfoRoute, _i, _len, _ref;

  processGraph = null;

  selectedVertexes = [];

  textInfoRoute = null;

  displayMessage = function(infotTitle, infoText, errorClass) {
    return $("#message").empty().build(function(b) {
      return b.div({
        "class": "alert alert-block " + errorClass + " fade in"
      }, function() {
        b.button("x", {
          "class": "close",
          "data-dismiss": "alert"
        });
        b.h4(infotTitle, {
          "class": "alert-heading"
        });
        return b.p(infoText);
      });
    });
  };

  cleanupRoutes = function() {
    var groupV, v, _i, _len;
    if (processGraph) {
      for (_i = 0, _len = processGraph.length; _i < _len; _i++) {
        v = processGraph[_i];
        stage.get("#" + v.Name)[0].setFill("#666");
        stage.get("#text_" + v.Name)[0].setFill("#fff");
        groupV = stage.get("#group_" + v.Name)[0];
        groupV.on("mouseover touchstart mouseout touchend click tap");
        setStyleOut(groupV);
        if (v.FromVertex) {
          stage.get("#edge_" + v.FromVertex + v.Name)[0].setStroke("#08C");
        }
      }
      processGraph = null;
      selectedVertexes = [];
      textInfoRoute.setText("Select two vertexes to trace a route.");
      return stage.draw();
    }
  };

  drawRoute = function(pathResult) {
    var colorFill, drawVertex, path, v, _i, _len;
    cleanupRoutes();
    processGraph = pathResult;
    colorFill = "#181818";
    drawVertex = function(vertex) {
      var edge;
      stage.get("#" + vertex.Name)[0].setFill(colorFill);
      stage.get("#text_" + vertex.Name)[0].setFill(colorFill);
      edge = stage.get("#edge_" + vertex.FromVertex + vertex.Name);
      if (edge && edge.length > 0) {
        return edge[0].setStroke(colorFill);
      }
    };
    if (processGraph.length <= 1) {
      cleanupRoutes();
      displayMessage("No such route!", "No route can be found for this destination.", "alert-info");
      return;
    }
    setStyleOver(stage.get("#group_" + processGraph[0].Name)[0]);
    setStyleOver(stage.get("#group_" + processGraph[processGraph.length - 1].Name)[0]);
    path = null;
    for (_i = 0, _len = processGraph.length; _i < _len; _i++) {
      v = processGraph[_i];
      drawVertex(v);
      path = path ? "" + path + " -> " : "";
      path = path + v.Name;
    }
    textInfoRoute.setText("Total distance: " + processGraph[processGraph.length - 1].DistanceFromStart + "\nPath: " + path);
    return stage.draw();
  };

  setStyleOver = function(groupShape) {
    var shape, _i, _len, _ref;
    _ref = groupShape.children;
    for (_i = 0, _len = _ref.length; _i < _len; _i++) {
      shape = _ref[_i];
      shape.setFill('#333');
    }
    groupShape.transitionTo({
      scale: {
        x: 1.3,
        y: 1.3
      },
      duration: 1,
      easing: 'elastic-ease-out'
    });
    return stage.draw();
  };

  setStyleOut = function(groupShape) {
    var shape, _i, _len, _ref;
    _ref = groupShape.children;
    for (_i = 0, _len = _ref.length; _i < _len; _i++) {
      shape = _ref[_i];
      shape.setFill('#666');
    }
    groupShape.transitionTo({
      scale: {
        x: 1,
        y: 1
      },
      duration: 1,
      easing: 'elastic-ease-out'
    });
    return stage.draw();
  };

  processShape = function(shape) {
    var item, _i, _len, _ref;
    if (shape.children) {
      _ref = shape.children;
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        item = _ref[_i];
        processShape(item);
      }
    }
    if (shape.nodeType === "Group") {
      shape.on("mouseover touchstart", function(evt) {
        return setStyleOver(this);
      });
      shape.on("mouseout touchend", function(evt) {
        return setStyleOut(this);
      });
      return shape.on("click tap", function(evt) {
        setStyleOver(this);
        this.off("mouseover touchstart mouseout touchend click tap");
        return selectedVertexes.push(this);
      });
    }
  };

  stage = Kinetic.Node.create($("#GraphJson").val(), 'map');

  stage.setWidth($("#sitemap-path").outerWidth());

  stage.setHeight(500);

  textInfoRoute = new Kinetic.Text({
    x: 2,
    y: 2,
    stroke: '#555',
    strokeWidth: 1,
    fill: '#ddd',
    text: "Select two vertexes to trace a route.",
    fontSize: 12,
    fontFamily: 'Calibri',
    textFill: '#555',
    alpha: 0.7,
    padding: 4,
    cornerRadius: 2
  });

  stage.children[0].add(textInfoRoute);

  stage.draw();

  _ref = stage.children;
  for (_i = 0, _len = _ref.length; _i < _len; _i++) {
    item = _ref[_i];
    processShape(item);
  }

  $("#calcule-route").click(function() {
    textInfoRoute.setText("Calculating...");
    stage.draw();
    return $.ajax({
      type: "POST",
      url: "/map/route",
      dataType: "json",
      traditional: true,
      data: {
        graph: $("#GraphJson").val(),
        steps: selectedVertexes.map(function(v) {
          return v.children[0].getId();
        })
      },
      success: function(data) {
        return drawRoute(data);
      },
      error: function(jqXHR, textStatus, errorThrown) {
        if (jqXHR.status === 404) {
          displayMessage(errorThrown, "You can not trace a route to this destination", "alert-error");
          textInfoRoute.setText("Press F5 to refresh this page and try another route");
          stage.draw();
          return cleanupRoutes();
        } else {
          return displayMessage(textStatus, errorThrown, "alert-error");
        }
      },
      fail: function(err, type, statusText) {
        displayMessage("Error when trying to calculate the route", statusText, "alert-error");
        textInfoRoute.setText("Try again");
        return stage.draw();
      }
    });
  });

}).call(this);
