processGraph = null
selectedVertexes = []
textInfoRoute = null

displayMessage = (infotTitle, infoText, errorClass) ->
	$("#message").empty().build (b) ->
		b.div
			class: "alert alert-block #{errorClass} fade in"
			,
			() ->
				b.button "x"
					class: "close"
					"data-dismiss": "alert"
				b.h4 infotTitle,
					class: "alert-heading"
				b.p infoText

cleanupRoutes = () ->
	if processGraph
		for v in processGraph
			stage.get("##{v.Name}")[0].setFill("#666")
			stage.get("#text_#{v.Name}")[0].setFill("#fff")
			groupV = stage.get("#group_#{v.Name}")[0]
			groupV.on "mouseover touchstart mouseout touchend click tap"
			setStyleOut groupV
			if v.FromVertex
				stage.get("#edge_#{v.FromVertex}#{v.Name}")[0].setStroke("#08C")
		processGraph = null
		selectedVertexes = []
		textInfoRoute.setText "Select two vertexes to trace a route."
		stage.draw()
	
drawRoute = (pathResult) ->
	cleanupRoutes()
	processGraph = pathResult
	colorFill = "#181818"

	# Draw routes method
	drawVertex = (vertex) ->
		stage.get("##{vertex.Name}")[0].setFill(colorFill)
		stage.get("#text_#{vertex.Name}")[0].setFill(colorFill)
		edge = stage.get("#edge_#{vertex.FromVertex}#{vertex.Name}")
		if edge and edge.length > 0
			edge[0].setStroke(colorFill)
				
			
	
	if processGraph.length <= 1
		cleanupRoutes()
		displayMessage "No such route!", "No route can be found for this destination.", "alert-info"
		return

	# Block ui
	setStyleOver stage.get("#group_#{processGraph[0].Name}")[0]
	setStyleOver stage.get("#group_#{processGraph[processGraph.length - 1].Name}")[0]

	# Draw routes
	path = null
	for v in processGraph
		drawVertex v 
		path = if path then "#{path} -> "  else ""
		path = path + v.Name
	textInfoRoute.setText "Total distance: #{processGraph[processGraph.length - 1].DistanceFromStart}\nPath: #{path}"
	stage.draw()

setStyleOver = (groupShape) ->
	shape.setFill('#333') for shape in groupShape.children
	groupShape.transitionTo
		scale:
			x: 1.3
			y: 1.3
		duration: 1
		easing: 'elastic-ease-out'
	stage.draw()

setStyleOut = (groupShape) ->
	shape.setFill('#666') for shape in groupShape.children
	groupShape.transitionTo
		scale:
			x: 1
			y: 1
		duration: 1
		easing: 'elastic-ease-out'
	stage.draw()

processShape = (shape) ->
	processShape item for item in shape.children if shape.children
	if shape.nodeType == "Group"
		shape.on("mouseover touchstart", (evt) -> setStyleOver(@))
		shape.on("mouseout touchend", (evt) -> setStyleOut(@))
		shape.on("click tap", (evt) -> 
			setStyleOver @
			@.off "mouseover touchstart mouseout touchend click tap"
			selectedVertexes.push @
			
		)

stage = Kinetic.Node.create $("#GraphJson").val(), 'map'
stage.setWidth $("#sitemap-path").outerWidth()
stage.setHeight 500

textInfoRoute = new Kinetic.Text
	x: 2
	y: 2
	stroke: '#555'
	strokeWidth: 1
	fill: '#ddd'
	text: "Select two vertexes to trace a route."
	fontSize: 12
	fontFamily: 'Calibri'
	textFill: '#555'
	alpha: 0.7
	padding: 4
	cornerRadius: 2

stage.children[0].add textInfoRoute
stage.draw()

processShape item for item in stage.children

# Events
$("#calcule-route").click () -> 
	textInfoRoute.setText "Calculating..."
	stage.draw()
	$.ajax
		type: "POST"
		url: "/map/route"
		dataType: "json"
		traditional: true
		data: 
			graph: $("#GraphJson").val()
			steps: selectedVertexes.map (v) -> v.children[0].getId()
		success: (data) -> 
			drawRoute data
		error: (jqXHR, textStatus, errorThrown) ->
			if jqXHR.status == 404
				displayMessage(errorThrown, "You can not trace a route to this destination", "alert-error")
				textInfoRoute.setText "Press F5 to refresh this page and try another route"
				stage.draw()
				cleanupRoutes()
			else
				displayMessage(textStatus, errorThrown, "alert-error")
		fail: (err, type, statusText) -> 
			displayMessage("Error when trying to calculate the route", statusText, "alert-error")
			textInfoRoute.setText "Try again"
			stage.draw()