(function() {
  var addEdge, addVertex, currentShapeOver, currentShapeSelected, getNextCha, layer, nextChaIdent, process, setStyleOut, setStyleOver, stage;

  nextChaIdent = 'A';

  currentShapeOver = null;

  currentShapeSelected = null;

  setStyleOver = function(groupShape) {
    var shape, _i, _len, _ref;
    _ref = groupShape.children;
    for (_i = 0, _len = _ref.length; _i < _len; _i++) {
      shape = _ref[_i];
      shape.setFill('#333');
    }
    groupShape.transitionTo({
      scale: {
        x: 1.3,
        y: 1.3
      },
      duration: 1,
      easing: 'elastic-ease-out'
    });
    return layer.draw();
  };

  setStyleOut = function(groupShape) {
    var shape, _i, _len, _ref;
    _ref = groupShape.children;
    for (_i = 0, _len = _ref.length; _i < _len; _i++) {
      shape = _ref[_i];
      shape.setFill('#666');
    }
    groupShape.transitionTo({
      scale: {
        x: 1,
        y: 1
      },
      duration: 1,
      easing: 'elastic-ease-out'
    });
    return layer.draw();
  };

  getNextCha = function() {
    var vertex;
    nextChaIdent = String.fromCharCode(nextChaIdent.charCodeAt(0) + 1);
    vertex = layer.get("#group_" + nextChaIdent);
    if (vertex.length > 0) {
      nextChaIdent = getNextCha();
    }
    return nextChaIdent;
  };

  process = function(from, to, distance) {
    var $map, fromVertex, toVertex, x, y;
    $map = $("#map");
    x = Math.floor((Math.random() * $map.width() - 15) + 15);
    y = Math.floor((Math.random() * $map.height() - 15) + 15);
    fromVertex = layer.get("#group_" + from);
    if (fromVertex.length > 0) {
      fromVertex = fromVertex[0];
    } else {
      fromVertex = addVertex(from, {
        x: x,
        y: y
      });
    }
    x = Math.floor((Math.random() * $map.width() - 15) + 15);
    y = Math.floor((Math.random() * $map.height() - 15) + 15);
    toVertex = layer.get("#group_" + to);
    if (toVertex.length > 0) {
      toVertex = toVertex[0];
    } else {
      toVertex = addVertex(to, {
        x: x,
        y: y
      });
    }
    return addEdge(fromVertex, toVertex, distance);
  };

  addEdge = function(shapeFrom, shapeTo, distance) {
    var line, lineTwoWay, nameFrom, nameTo, posCenter, posDest, posOrigem, text;
    posOrigem = shapeFrom.getPosition();
    posDest = shapeTo.getPosition();
    posCenter = {
      x: (posDest.x + posOrigem.x) / 2,
      y: (posDest.y + posOrigem.y) / 2
    };
    nameFrom = shapeFrom.children[0].getId();
    nameTo = shapeTo.children[0].getId();
    lineTwoWay = stage.get("#edge_" + nameTo + nameFrom);
    if (lineTwoWay && lineTwoWay.length > 0) {
      lineTwoWay = lineTwoWay[0];
      if (!distance) {
        distance = parseInt(lineTwoWay.attrs.distance);
      }
      text = stage.get("#textDistance_" + nameTo + nameFrom);
    } else {
      lineTwoWay = null;
    }
    if (!lineTwoWay && !distance) {
      distance = Math.sqrt(Math.pow(posDest.x - posOrigem.x, 2) + Math.pow(posDest.y - posOrigem.y, 2));
      distance = Math.round(distance);
      if ($("#prompt-distances > input").is(":checked")) {
        distance = prompt("Distance between vertices: ", distance);
      }
    }
    line = new Kinetic.Line({
      stroke: "#08C",
      strokeWidth: 6,
      points: [posOrigem.x, posOrigem.y, posDest.x, posDest.y],
      name: "edge",
      id: "edge_" + nameFrom + nameTo
    });
    line.setAttrs({
      distance: distance,
      fromVertex: nameFrom,
      toVertex: nameTo
    });
    if (lineTwoWay && text && text.length > 0) {
      text[0].setText("" + nameTo + " <> " + nameFrom + ": " + distance);
    } else {
      text = new Kinetic.Text({
        x: posCenter.x,
        y: posCenter.y,
        text: "" + nameFrom + " > " + nameTo + ": " + distance,
        fontSize: 10,
        fontFamily: "Calibri",
        stroke: "#333",
        cornerRadius: 5,
        strokeWidth: 1,
        textFill: "#fff",
        fill: "#08C",
        align: "center",
        padding: 2,
        id: "textDistance_" + nameFrom + nameTo
      });
      layer.add(text);
    }
    layer.add(line);
    line.moveToBottom();
    layer.draw();
    setStyleOut(shapeFrom);
    setStyleOut(shapeTo);
    return currentShapeSelected = null;
  };

  addVertex = function(letter, posMouse) {
    var circle, group, posX, posY, text;
    if (letter == null) {
      letter = nextChaIdent;
    }
    if (posMouse == null) {
      posMouse = stage.getMousePosition();
    }
    if (currentShapeOver) {
      return;
    }
    posX = posMouse.x;
    posY = posMouse.y;
    group = new Kinetic.Group({
      x: posX,
      y: posY,
      id: "group_" + letter
    });
    circle = new Kinetic.Circle({
      radius: 20,
      fill: "#666",
      stroke: "black",
      strokeWidth: 1,
      name: "vertex",
      id: letter
    });
    text = new Kinetic.Text({
      x: -5,
      y: -5,
      text: letter,
      fontSize: 12,
      fontFamily: "Calibri",
      fontStyle: "bold",
      textFill: "#fff",
      align: "center",
      id: "text_" + letter
    });
    group.add(circle);
    group.add(text);
    group.on("mouseover touchstart", function(evt) {
      setStyleOver(this);
      return currentShapeOver = this;
    });
    group.on("mouseout touchend", function(evt) {
      currentShapeOver = null;
      if ((currentShapeSelected && currentShapeSelected.getId() !== this.getId()) || !currentShapeSelected) {
        return setStyleOut(this);
      }
    });
    group.on("click tap", function(evt) {
      if (currentShapeSelected) {
        if (currentShapeSelected.getId() !== this.getId()) {
          return addEdge(currentShapeSelected, this);
        } else {
          setStyleOut(currentShapeSelected);
          return currentShapeSelected = null;
        }
      } else {
        setStyleOver(this);
        return currentShapeSelected = currentShapeOver;
      }
    });
    layer.add(group);
    stage.draw();
    nextChaIdent = getNextCha();
    return group;
  };

  $("#prompt-distances").tooltip({
    title: "Check to enable prompt distance"
  });

  $("#form-map button").click(function(e) {
    e.preventDefault();
    $("#GraphJson").val(stage.toJSON());
    return $("#form-map").submit();
  });

  $("#reset-map").click(function(e) {
    e.preventDefault();
    layer.removeChildren();
    nextChaIdent = 'A';
    currentShapeOver = null;
    currentShapeSelected = null;
    return layer.draw();
  });

  $("#text-graph").bind("keyup change", function(e) {
    var distance, edges, _i, _len, _results;
    if ((e.type === "keyup" && e.keyCode === 13) || e.type === "change") {
      edges = e.target.value.split(/(?:,| )+/);
      _results = [];
      for (_i = 0, _len = edges.length; _i < _len; _i++) {
        e = edges[_i];
        distance = e.replace(/[^\d.]/g, "");
        e = e.replace(/[\d.]/g, "");
        _results.push(process(e[0], e[1], distance));
      }
      return _results;
    }
  });

  stage = new Kinetic.Stage({
    container: "map",
    width: $("#sitemap-path").outerWidth(),
    height: 500
  });

  layer = new Kinetic.Layer();

  stage.add(layer);

  stage.getContainer().addEventListener("click", function() {
    return addVertex();
  });

}).call(this);
