(function() {

  $('#create-map').popover({
    title: "Create a new map",
    content: "Create a map with the vertices and edges to calculate the route"
  });

  $('#search-map').popover({
    title: "Map list",
    content: "Search for maps in our database to calculate the route."
  });

}).call(this);
