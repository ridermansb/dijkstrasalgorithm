﻿using System.Web;
using System.Web.Mvc;

namespace DijkstraAlgorithm.Web.Extension
{
	public static class ControllerExtensions
	{
		public static MvcHtmlString TitlePage(this SiteMapNode node)
		{
			if (node == null || string.IsNullOrWhiteSpace(node.Title))
				return new MvcHtmlString("Dijkstra Algorithm");
			return new MvcHtmlString(node.Title + " - Dijkstra Algorithm");
		}
	}
}