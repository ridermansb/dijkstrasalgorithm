[assembly: WebActivator.PreApplicationStartMethod(typeof(DijkstraAlgorithm.Web.App_Start.NinjectWebCommon), "Start")]
[assembly: WebActivator.ApplicationShutdownMethodAttribute(typeof(DijkstraAlgorithm.Web.App_Start.NinjectWebCommon), "Stop")]

namespace DijkstraAlgorithm.Web.App_Start
{
	using System;
	using System.Linq;
	using System.Web;
	using DijkstraAlgorithm.Model;
	using DijkstraAlgorithm.Repository;
	using Microsoft.Web.Infrastructure.DynamicModuleHelper;
	using Ninject;
	using Ninject.Web.Common;
	using Raven.Client;
	using Raven.Client.Document;
	using Raven.Client.Indexes;

	public static class NinjectWebCommon
	{
		private static readonly Bootstrapper bootstrapper = new Bootstrapper();

		/// <summary>
		/// Starts the application
		/// </summary>
		public static void Start()
		{
			DynamicModuleUtility.RegisterModule(typeof(OnePerRequestHttpModule));
			DynamicModuleUtility.RegisterModule(typeof(NinjectHttpModule));
			bootstrapper.Initialize(CreateKernel);
		}

		/// <summary>
		/// Stops the application.
		/// </summary>
		public static void Stop()
		{
			bootstrapper.ShutDown();
		}

		/// <summary>
		/// Creates the kernel that will manage your application.
		/// </summary>
		/// <returns>The created kernel.</returns>
		private static IKernel CreateKernel()
		{
			var kernel = new StandardKernel();
			kernel.Bind<Func<IKernel>>().ToMethod(ctx => () => new Bootstrapper().Kernel);
			kernel.Bind<IHttpModule>().To<HttpApplicationInitializationHttpModule>();

			RegisterServices(kernel);
			return kernel;
		}

		/// <summary>
		/// Load your modules or register your services here!
		/// </summary>
		/// <param name="kernel">The kernel.</param>
		private static void RegisterServices(IKernel kernel)
		{
			kernel.Bind<IDocumentStore>().ToMethod(context =>
			{
				var documentStore = new DocumentStore() { ConnectionStringName = "RavenDB" };
				documentStore.Initialize();
				var idx = documentStore.DatabaseCommands.GetIndex("Map/SearchByName");
				if (idx == null)
					documentStore.DatabaseCommands.PutIndex("Map/SearchByName", new IndexDefinitionBuilder<Map>
					{
						Map = maps => from map in maps
									  select new { map.Name },
						Analyzers = { { x => x.Name, "SimpleAnalyzer" } }
					});
				return documentStore;
			}).InSingletonScope();

			kernel.Bind<IDocumentSession>().ToMethod(context => context.Kernel.Get<IDocumentStore>().OpenSession()).InRequestScope();
			kernel.Bind<IRepository>().To<RavenDBRepository>().InRequestScope();
		}
	}
}
