﻿using System.Web.Mvc;
using Cassette.Views;
using DijkstraAlgorithm.Web.Extension;
using DijkstraAlgorithm.Web.Infra.ActionResults;
using MvcSiteMapProvider;

namespace DijkstraAlgorithm.Web.Controllers
{
    public abstract class baseController : Controller
    {
		protected override void OnResultExecuting(ResultExecutingContext filterContext)
		{
			Bundles.Reference("Content");
			Bundles.Reference("Scripts/jquery-1.7.2.min.js", "top");
			Bundles.Reference("Scripts");

			var controllerName = filterContext.RouteData.GetRequiredString("Controller");
			var actionName = filterContext.RouteData.GetRequiredString("Action");
			
			string urlBundle = "{0}/Controllers/{1}/{2}.{3}";
			var jsFile = string.Format(urlBundle, "Scripts", controllerName, actionName, "js");
			var cssFile = string.Format(urlBundle, "Content", controllerName, actionName, "css");
			
			if (System.IO.File.Exists(Server.MapPath("~/" + jsFile)))
				Bundles.Reference(jsFile, "page");
			if (System.IO.File.Exists(Server.MapPath("~/" + cssFile)))
				Bundles.Reference(cssFile, "page");

			base.OnResultExecuting(filterContext);
		}

		protected override void OnActionExecuting(ActionExecutingContext filterContext)
		{
			ViewBag.Title = this.GetCurrentSiteMapNode().TitlePage();
			base.OnActionExecuting(filterContext);
		}

		protected internal GraphProcessedJsonResult GraphProcessedJson(IProcessGraph graphProcessed)
		{
			return new GraphProcessedJsonResult(graphProcessed);
		}
    }
}
