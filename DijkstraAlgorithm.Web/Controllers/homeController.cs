﻿using System.Web.Mvc;

namespace DijkstraAlgorithm.Web.Controllers
{
	[OutputCache(CacheProfile = "Fixed")]
	public class homeController : baseController
    {
        public ActionResult index()
        {
            return View();
        }
    }
}
