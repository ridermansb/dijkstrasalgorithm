﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using DijkstraAlgorithm.Model;
using DijkstraAlgorithm.Repository;
using DijkstraAlgorithm.Web.Infra.ActionResults;

namespace DijkstraAlgorithm.Web.Controllers
{
    public class mapController : baseController
    {
		private readonly IRepository _repository;
		public mapController(IRepository repository)
		{
			_repository = repository;
		}

        public ActionResult index(string search)
        {
			var query = _repository.Query<Map>();
			if (!string.IsNullOrWhiteSpace(search))
			{
				search = search.ToLower();
				query = query.Where(p => p.Name == search);
			}

			return View(query.OrderByDescending(p => p.DateAdded).ToList());
        }

		[OutputCache(CacheProfile = "Fixed")]
		public ActionResult create()
		{
			return View();
		}

		[ValidateAntiForgeryToken]
		[HttpPost]
		public ActionResult create(Map model)
		{
			if (ModelState.IsValid)
			{
				if (!_repository.Query<Map>().Any(p => p.Name == model.Name))
				{
					_repository.Store(model);
					_repository.SaveChanges();
					return RedirectToAction("details", new { id= model.Id });
				}
				ModelState.AddModelError("name", "Name already exists!");
			}

			return View(model);
		}

		[OutputCache(CacheProfile = "Fixed")]
		public ActionResult details(int id)
		{
			var model = _repository.Get<Map>(id);
			if (model == null)
				return HttpNotFound("Map not found.");
			return View(model);
		}

		[HttpPost]
		[OutputCache(CacheProfile = "Fixed")]
		public GraphProcessedJsonResult route(Graph graph, IList<string> steps)
		{
			if (steps == null || steps.Count < 2)
			{
				Response.StatusCode = 406;
				Response.StatusDescription = "You must select at least two vertices to calculate a route";
				return null;
			}
			var x = new MultiProcessGraph(graph, steps.ToArray());
			return GraphProcessedJson(x);
		}
    }
}
