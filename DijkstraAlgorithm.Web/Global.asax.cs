﻿using System.Web.Mvc;
using System.Web.Routing;
using DijkstraAlgorithm.Web.Infra.Binders;
using MvcSiteMapProvider.Web;

namespace DijkstraAlgorithm.Web
{
	public class MvcApplication : System.Web.HttpApplication
	{
		protected void Application_Start()
		{
			ViewEngines.Engines.Clear();
			ViewEngines.Engines.Add(new RazorViewEngine());

			AreaRegistration.RegisterAllAreas();
			XmlSiteMapController.RegisterRoutes(RouteTable.Routes);

			ModelBinders.Binders.Add(typeof(Graph), new GraphModelBinder());

			FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
			RouteConfig.RegisterRoutes(RouteTable.Routes);
		}
	}
}