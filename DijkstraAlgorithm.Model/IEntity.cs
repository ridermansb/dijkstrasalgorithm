﻿using System;

namespace DijkstraAlgorithm.Model
{
	public interface IEntity
	{
		int Id { get; set; }
		DateTime DateAdded { get; }
	}
}
