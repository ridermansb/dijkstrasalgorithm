﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DijkstraAlgorithm.Model
{
	public class Map : IEntity
	{
		public int Id { get; set; }
		public string Name { get; set; }
		public string GraphJson { get; set; }
		public DateTime DateAdded { get; private set; }

		public Map()
		{
			DateAdded = DateTime.Now;
		}
	}
}
